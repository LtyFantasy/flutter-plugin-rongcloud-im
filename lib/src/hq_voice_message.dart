import 'voice_message.dart';

class HQVoiceMessage extends VoiceMessage {
  static const String objectName = "RC:HQVCMsg";

  @override
  String getObjectName() {
    return objectName;
  }
}