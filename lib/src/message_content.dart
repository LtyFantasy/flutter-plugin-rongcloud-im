class MessageContent implements MessageCoding, MessageContentView {
  @override
  void decode(String jsonStr) {}

  @override
  String? encode() => null;

  @override
  String? conversationDigest() => null;

  @override
  String? getObjectName() => null;
}

class MessageCoding {
  String? encode() => null;

  void decode(String jsonStr) {}

  String? getObjectName() => null;
}

class MessageContentView {
  String? conversationDigest() => null;
}
