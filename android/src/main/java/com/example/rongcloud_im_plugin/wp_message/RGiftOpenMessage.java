package com.example.rongcloud_im_plugin.wp_message;

import android.os.Parcel;
import android.text.TextUtils;
import android.util.Log;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;

import java.io.UnsupportedEncodingException;

import io.rong.common.ParcelUtils;
import io.rong.common.RLog;
import io.rong.imlib.MessageTag;
import io.rong.imlib.model.MessageContent;

@MessageTag(value = "WP:GiftOpenNotification", flag = MessageTag.ISCOUNTED | MessageTag.ISPERSISTED)
public class RGiftOpenMessage extends MessageContent {

    private String message;
    private String gift_id;
    private String content;
    private String name;
    private String gender;
    private String extra;
    private RUserInfoBean user;

    public RGiftOpenMessage() {
    }

    public RGiftOpenMessage(Parcel in) {
        setMessage(ParcelUtils.readFromParcel(in));
        setGift_id(ParcelUtils.readFromParcel(in));
        setContent(ParcelUtils.readFromParcel(in));
        setGender(ParcelUtils.readFromParcel(in));
        setExtra(ParcelUtils.readFromParcel(in));
        setName(ParcelUtils.readFromParcel(in));
        setUser(JSONObject.parseObject(ParcelUtils.readFromParcel(in), RUserInfoBean.class));
    }

    public RGiftOpenMessage(byte[] data) {
        String jsonStr = null;
        try {
            jsonStr = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            RLog.e("JSONException", e.getMessage());
        }
        Log.d("RGiftOpenMessage", "RGiftOpenMessage: " + jsonStr);
        RGiftOpenMessage rGiftOpenMessage = JSONObject.parseObject(jsonStr, RGiftOpenMessage.class);
        setMessage(rGiftOpenMessage.getMessage());
        setGift_id(rGiftOpenMessage.getGift_id());
        setContent(rGiftOpenMessage.getContent());
        setGender(rGiftOpenMessage.getGender());
        setExtra(rGiftOpenMessage.getExtra());
        setName(rGiftOpenMessage.getName());
        setUser(rGiftOpenMessage.getUser());
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public byte[] encode() {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("message", getMessage());
            jsonObj.put("gift_id", getGift_id());
            jsonObj.put("content", getContent());
            jsonObj.put("gender", getGender());
            jsonObj.put("name", getName());
            if (!TextUtils.isEmpty(getExtra()))
                jsonObj.put("extra", getExtra());
            if (getUser() != null) jsonObj.put("user", getUser());
        } catch (JSONException e) {
            RLog.e("JSONException", e.getMessage());
        }
        try {
            return jsonObj.toString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelUtils.writeToParcel(dest, getMessage());
        ParcelUtils.writeToParcel(dest, getGift_id());
        ParcelUtils.writeToParcel(dest, getContent());
        ParcelUtils.writeToParcel(dest, getGender());
        ParcelUtils.writeToParcel(dest, getExtra());
        ParcelUtils.writeToParcel(dest, getName());
        ParcelUtils.writeToParcel(dest, JSONObject.toJSONString(getUser()));
    }

    public static final Creator<RGiftOpenMessage> CREATOR = new Creator<RGiftOpenMessage>() {
        @Override
        public RGiftOpenMessage createFromParcel(Parcel source) {
            return new RGiftOpenMessage(source);
        }

        @Override
        public RGiftOpenMessage[] newArray(int size) {
            return new RGiftOpenMessage[size];
        }
    };

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getGift_id() {
        return gift_id;
    }

    public void setGift_id(String gift_id) {
        this.gift_id = gift_id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public RUserInfoBean getUser() {
        return user;
    }

    public void setUser(RUserInfoBean user) {
        this.user = user;
    }
}
