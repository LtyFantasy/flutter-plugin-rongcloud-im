package com.example.rongcloud_im_plugin.wp_message;

import android.os.Parcel;
import android.text.TextUtils;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;

import java.io.UnsupportedEncodingException;

import io.rong.common.ParcelUtils;
import io.rong.common.RLog;
import io.rong.imlib.MessageTag;
import io.rong.message.InformationNotificationMessage;

@MessageTag(value = "WP:ConversationUnmatch", flag = MessageTag.NONE)
public class RUnMatchMessage extends InformationNotificationMessage {

    private String message;
    private long created_at;

    public RUnMatchMessage() {}

    public RUnMatchMessage(Parcel in) {
        setMessage(ParcelUtils.readFromParcel(in));
        setCreated_at(ParcelUtils.readLongFromParcel(in));
        setExtra(ParcelUtils.readFromParcel(in));
    }

    public RUnMatchMessage(byte[] data) {
        String jsonStr = null;
        try {
            jsonStr = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            RLog.e("JSONException", e.getMessage());
        }
        RUnMatchMessage rKickMessage = JSONObject.parseObject(jsonStr, RUnMatchMessage.class);
        setMessage(rKickMessage.getMessage());
        setCreated_at(rKickMessage.getCreated_at());
        setExtra(rKickMessage.getExtra());
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public byte[] encode() {
        JSONObject jsonObj = new JSONObject();
        try {

            jsonObj.put("message", getMessage());
            jsonObj.put("created_at", getCreated_at());
            if (!TextUtils.isEmpty(getExtra()))
                jsonObj.put("extra", getExtra());
        } catch (JSONException e) {
            RLog.e("JSONException", e.getMessage());
        }
        try {
            return jsonObj.toString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelUtils.writeToParcel(dest, getMessage());
        ParcelUtils.writeToParcel(dest, getCreated_at());
        ParcelUtils.writeToParcel(dest, getExtra());
    }

    public static final Creator<RUnMatchMessage> CREATOR = new Creator<RUnMatchMessage>() {
        @Override
        public RUnMatchMessage createFromParcel(Parcel source) {
            return new RUnMatchMessage(source);
        }
        @Override
        public RUnMatchMessage[] newArray(int size) {
            return new RUnMatchMessage[size];
        }
    };

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(long created_at) {
        this.created_at = created_at;
    }

}
