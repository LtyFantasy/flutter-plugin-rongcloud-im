package com.example.rongcloud_im_plugin.wp_message;

import android.os.Parcel;
import android.text.TextUtils;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;

import java.io.UnsupportedEncodingException;

import io.rong.common.ParcelUtils;
import io.rong.common.RLog;
import io.rong.imlib.MessageTag;
import io.rong.message.InformationNotificationMessage;

/**
 * Created by guotengqian on 2017/5/26 17:17.
 * def
 */
@MessageTag(value = "WP:ReadStutasTip", flag = MessageTag.ISPERSISTED)
public class RReadStatusTipMessage extends InformationNotificationMessage {

    private String message;

    public RReadStatusTipMessage() {}

    public RReadStatusTipMessage(Parcel in) {
        setMessage(ParcelUtils.readFromParcel(in));
        setExtra(ParcelUtils.readFromParcel(in));
    }

    public RReadStatusTipMessage(byte[] data) {
        String jsonStr = null;
        try {
            jsonStr = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            RLog.e("JSONException", e.getMessage());
        }
        RReadStatusTipMessage rGiftOpenMessage = JSONObject.parseObject(jsonStr, RReadStatusTipMessage.class);
        setMessage(rGiftOpenMessage.getMessage());
        setExtra(rGiftOpenMessage.getExtra());
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public byte[] encode() {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("message", getMessage());
            if (!TextUtils.isEmpty(getExtra()))
                jsonObj.put("extra", getExtra());
        } catch (JSONException e) {
            RLog.e("JSONException", e.getMessage());
        }
        try {
            return jsonObj.toString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelUtils.writeToParcel(dest, getMessage());
        ParcelUtils.writeToParcel(dest, getExtra());
    }

    public static final Creator<RReadStatusTipMessage> CREATOR = new Creator<RReadStatusTipMessage>() {
        @Override
        public RReadStatusTipMessage createFromParcel(Parcel source) {
            return new RReadStatusTipMessage(source);
        }
        @Override
        public RReadStatusTipMessage[] newArray(int size) {
            return new RReadStatusTipMessage[size];
        }
    };

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static String getObjectName(){
        return "WP:ReadStutasTip";
    }

}

