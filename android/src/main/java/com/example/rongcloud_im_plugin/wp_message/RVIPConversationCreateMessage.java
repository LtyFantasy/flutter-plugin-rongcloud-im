package com.example.rongcloud_im_plugin.wp_message;

import android.os.Parcel;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;

import java.io.UnsupportedEncodingException;

import io.rong.common.ParcelUtils;
import io.rong.common.RLog;
import io.rong.imlib.MessageTag;
import io.rong.message.InformationNotificationMessage;

@MessageTag(value = "WP:VIPConversationStartCommand", flag = MessageTag.NONE)
public class RVIPConversationCreateMessage extends InformationNotificationMessage {
    private static final String TAG = "RVIPConversationCreateMessage";

    private RUserInfoBean user;

    public RVIPConversationCreateMessage() {
    }

    public RVIPConversationCreateMessage(Parcel in) {
        try {
            setUser(JSONObject.parseObject(ParcelUtils.readFromParcel(in), RUserInfoBean.class));
        } catch (Exception e) {
            RLog.e(TAG, e.getMessage());
        }
    }

    public RVIPConversationCreateMessage(byte[] data) {
        String jsonStr = null;
        try {
            jsonStr = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            RLog.e("JSONException", e.getMessage());
        }
        RVIPConversationCreateMessage rvipConversationCreateMessage = JSONObject.parseObject(jsonStr, RVIPConversationCreateMessage.class);
        setUser(rvipConversationCreateMessage.getUser());
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public byte[] encode() {
        JSONObject jsonObj = new JSONObject();
        try {
            if (getUser() != null) jsonObj.put("user", getUser());
        } catch (JSONException e) {
            RLog.e("JSONException", e.getMessage());
        }
        try {
            return jsonObj.toString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelUtils.writeToParcel(dest, JSONObject.toJSONString(getUser()));
    }


    public static final Creator<RVIPConversationCreateMessage> CREATOR = new Creator<RVIPConversationCreateMessage>() {
        @Override
        public RVIPConversationCreateMessage createFromParcel(Parcel source) {
            return new RVIPConversationCreateMessage(source);
        }

        @Override
        public RVIPConversationCreateMessage[] newArray(int size) {
            return new RVIPConversationCreateMessage[size];
        }
    };

    public RUserInfoBean getUser() {
        return user;
    }

    public void setUser(RUserInfoBean user) {
        this.user = user;
    }

}
