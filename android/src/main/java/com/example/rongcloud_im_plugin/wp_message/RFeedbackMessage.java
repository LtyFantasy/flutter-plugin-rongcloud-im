package com.example.rongcloud_im_plugin.wp_message;

import android.os.Parcel;
import android.text.TextUtils;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;

import java.io.UnsupportedEncodingException;

import io.rong.common.ParcelUtils;
import io.rong.common.RLog;
import io.rong.imlib.MessageTag;
import io.rong.imlib.model.MessageContent;

@SuppressWarnings("JavadocReference")
@MessageTag(value = "WP:Feedback", flag = MessageTag.ISCOUNTED | MessageTag.ISPERSISTED)
public class RFeedbackMessage extends MessageContent {
    
    private String content;
    private String extra;
    private int type;
    private long created_at;
    private String feedback_id;
    
    public RFeedbackMessage() {}
    
    /**
     * 构造函数。
     * @param in 初始化传入的 Parcel。
     */
    public RFeedbackMessage(Parcel in) {
        setContent(ParcelUtils.readFromParcel(in));
        setType(ParcelUtils.readIntFromParcel(in));
        setExtra(ParcelUtils.readFromParcel(in));
        setCreated_at(ParcelUtils.readLongFromParcel(in));
        setFeedback_id(ParcelUtils.readFromParcel(in));
    }
    
    /**
     * 构造函数。
     * @param data 初始化传入的二进制数据。
     * @param message 此参数代码中并没有调用，后续将废弃。
     */
    public RFeedbackMessage(byte[] data) {
        String jsonStr = null;
        try {
            jsonStr = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            RLog.e("JSONException", e.getMessage());
        }
        RFeedbackMessage rFeedbackMessage = JSONObject.parseObject(jsonStr, RFeedbackMessage.class);
        setContent(rFeedbackMessage.getContent());
        setType(rFeedbackMessage.getType());
        setExtra(rFeedbackMessage.getExtra());
        setCreated_at(rFeedbackMessage.getCreated_at());
        setFeedback_id(rFeedbackMessage.getFeedback_id());
    }
    
    /**
     * 描述了包含在 Parcelable 对象排列信息中的特殊对象的类型。
     * @return 一个标志位，表明 Parcelable 对象特殊对象类型集合的排列。
     */
    public int describeContents() {
        return 0;
    }
    
    /**
     * 将本地消息对象序列化为消息数据。
     * @return 消息数据。
     */
    @Override
    public byte[] encode() {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("content", getContent());
            jsonObj.put("type", getType());
            if (!TextUtils.isEmpty(getExtra()))
                jsonObj.put("extra", getExtra());
            jsonObj.put("created_at", getCreated_at());
            jsonObj.put("feedback_id", getFeedback_id());
        } catch (JSONException e) {
            RLog.e("JSONException", e.getMessage());
        }
        try {
            return jsonObj.toString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    /**
     * 将类的数据写入外部提供的 Parcel 中。
     * @param dest  对象被写入的 Parcel。
     * @param flags 对象如何被写入的附加标志，可能是 0 或 PARCELABLE_WRITE_RETURN_VALUE。
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelUtils.writeToParcel(dest, getContent());
        ParcelUtils.writeToParcel(dest, getType());
        ParcelUtils.writeToParcel(dest, getExtra());
        ParcelUtils.writeToParcel(dest, getCreated_at());
        ParcelUtils.writeToParcel(dest, getFeedback_id());
    }
    
    /**
     * 读取接口，目的是要从Parcel中构造一个实现了Parcelable的类的实例处理。
     */
    public static final Creator<RFeedbackMessage> CREATOR = new Creator<RFeedbackMessage>() {
        @Override
        public RFeedbackMessage createFromParcel(Parcel source) {
            return new RFeedbackMessage(source);
        }
        @Override
        public RFeedbackMessage[] newArray(int size) {
            return new RFeedbackMessage[size];
        }
    };
    
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    public int getType() {
        return type;
    }
    public void setType(int type) {
        this.type = type;
    }
    public String getExtra() {
        return extra;
    }
    public void setExtra(String extra) {
        this.extra = extra;
    }
    public long getCreated_at() {
        return created_at;
    }
    public void setCreated_at(long created_at) {
        this.created_at = created_at;
    }
    public String getFeedback_id() {
        return feedback_id;
    }
    public void setFeedback_id(String feedback_id) {
        this.feedback_id = feedback_id;
    }
    
}
