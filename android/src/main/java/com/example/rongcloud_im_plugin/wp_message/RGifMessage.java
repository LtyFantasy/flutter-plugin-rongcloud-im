package com.example.rongcloud_im_plugin.wp_message;

import android.os.Parcel;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;

import java.io.UnsupportedEncodingException;

import io.rong.common.ParcelUtils;
import io.rong.common.RLog;
import io.rong.imlib.MessageTag;
import io.rong.imlib.model.MessageContent;

/**
 * WooPlus Gif消息类型
 */
@MessageTag(value = "WP:Gif", flag = MessageTag.ISCOUNTED | MessageTag.ISPERSISTED)
public class RGifMessage extends MessageContent {
    private static final String TAG = "RGifMessage";

    private String content;
    private String url;
    private double width;
    private double height;
    private String extra;

    public RGifMessage() {
    }

    public RGifMessage(Parcel in) {
        try {
            setContent(ParcelUtils.readFromParcel(in));
            setUrl(ParcelUtils.readFromParcel(in));
            setWidth(ParcelUtils.readDoubleFromParcel(in));
            setHeight(ParcelUtils.readDoubleFromParcel(in));
            setExtra(ParcelUtils.readFromParcel(in));
        } catch (Exception e) {
            RLog.e(TAG, e.getMessage());
        }
    }

    public RGifMessage(byte[] data) {
        String jsonStr = null;
        try {
            jsonStr = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            RLog.e("JSONException", e.getMessage());
        }
        RGifMessage rGifMessage = JSONObject.parseObject(jsonStr, RGifMessage.class);
        setContent(rGifMessage.getContent());
        setUrl(rGifMessage.getUrl());
        setWidth(rGifMessage.getWidth());
        setHeight(rGifMessage.getHeight());
        setExtra(rGifMessage.getExtra());
    }

    /**
     * 读取接口，目的是要从Parcel中构造一个实现了Parcelable的类的实例处理。
     */
    public static final Creator<RGifMessage> CREATOR = new Creator<RGifMessage>() {
        @Override
        public RGifMessage createFromParcel(Parcel source) {
            return new RGifMessage(source);
        }

        @Override
        public RGifMessage[] newArray(int size) {
            return new RGifMessage[size];
        }
    };

    /**
     * 将类的数据写入外部提供的 Parcel 中。
     *
     * @param dest  对象被写入的 Parcel。
     * @param flags 对象如何被写入的附加标志，可能是 0 或 PARCELABLE_WRITE_RETURN_VALUE。
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelUtils.writeToParcel(dest, getContent());
        ParcelUtils.writeToParcel(dest, getUrl());
        ParcelUtils.writeToParcel(dest, getWidth());
        ParcelUtils.writeToParcel(dest, getHeight());
        ParcelUtils.writeToParcel(dest, getExtra());
    }

    /**
     * 描述了包含在 Parcelable 对象排列信息中的特殊对象的类型。
     *
     * @return 一个标志位，表明 Parcelable 对象特殊对象类型集合的排列。
     */
    public int describeContents() {
        return 0;
    }

    /**
     * 将本地消息对象序列化为消息数据。
     *
     * @return 消息数据。
     */
    @Override
    public byte[] encode() {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("content", getContent());
            jsonObj.put("url", getUrl());
            jsonObj.put("width", getWidth());
            jsonObj.put("height", getHeight());
            jsonObj.put("extra", getExtra());
        } catch (JSONException e) {
            RLog.e("JSONException", e.getMessage());
        }
        try {
            return jsonObj.toString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }
}
