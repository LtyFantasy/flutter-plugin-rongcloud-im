package com.example.rongcloud_im_plugin.wp_message;

import android.os.Parcel;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;

import java.io.UnsupportedEncodingException;

import io.rong.common.ParcelUtils;
import io.rong.common.RLog;
import io.rong.imlib.MessageTag;
import io.rong.imlib.model.MessageContent;

@MessageTag(value = "WP:FlashChatQuestion", flag = MessageTag.ISCOUNTED | MessageTag.ISPERSISTED)
public class RFlashChatQuestionMessage extends MessageContent {
    private static final String TAG = "RFlashChatQuestionMessage";

    String data;

    public RFlashChatQuestionMessage() {
    }

    public RFlashChatQuestionMessage(Parcel in) {
        try {
            setData(ParcelUtils.readFromParcel(in));
        } catch (Exception e) {
            RLog.e(TAG, e.getMessage());
        }
    }

    public RFlashChatQuestionMessage(byte[] data) {
        String jsonStr = null;
        try {
            jsonStr = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            RLog.e("JSONException", e.getMessage());
        }
        RFlashChatQuestionMessage rFlashChatQuestionMessage = JSONObject.parseObject(jsonStr, RFlashChatQuestionMessage.class);
        setData(rFlashChatQuestionMessage.getData());
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public byte[] encode() {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("data", getData());
        } catch (JSONException e) {
            RLog.e("JSONException", e.getMessage());
        }
        try {
            return jsonObj.toString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelUtils.writeToParcel(dest, getData());
    }

    public static final Creator<RFlashChatQuestionMessage> CREATOR = new Creator<RFlashChatQuestionMessage>() {
        @Override
        public RFlashChatQuestionMessage createFromParcel(Parcel source) {
            return new RFlashChatQuestionMessage(source);
        }

        @Override
        public RFlashChatQuestionMessage[] newArray(int size) {
            return new RFlashChatQuestionMessage[size];
        }
    };

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
