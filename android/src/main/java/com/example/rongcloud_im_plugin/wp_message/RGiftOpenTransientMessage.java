package com.example.rongcloud_im_plugin.wp_message;

import android.os.Parcel;
import android.text.TextUtils;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;

import java.io.UnsupportedEncodingException;

import io.rong.common.ParcelUtils;
import io.rong.common.RLog;
import io.rong.imlib.MessageTag;
import io.rong.message.InformationNotificationMessage;

@MessageTag(value = "WP:GiftOpenCommand", flag = MessageTag.NONE)
public class RGiftOpenTransientMessage extends InformationNotificationMessage {
    
    private String message;
    private String gift_id;
    private String gift_name;
    
    public RGiftOpenTransientMessage() {}
    
    public RGiftOpenTransientMessage(Parcel in) {
        setMessage(ParcelUtils.readFromParcel(in));
        setGift_id(ParcelUtils.readFromParcel(in));
        setExtra(ParcelUtils.readFromParcel(in));
        setGift_name(ParcelUtils.readFromParcel(in));
    }
    
    public RGiftOpenTransientMessage(byte[] data) {
        String jsonStr = null;
        try {
            jsonStr = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            RLog.e("JSONException", e.getMessage());
        }
        RGiftOpenTransientMessage rGiftOpenMessage = JSONObject.parseObject(jsonStr, RGiftOpenTransientMessage.class);
        setMessage(rGiftOpenMessage.getMessage());
        setGift_id(rGiftOpenMessage.getGift_id());
        setExtra(rGiftOpenMessage.getExtra());
        setGift_name(rGiftOpenMessage.getGift_name());
    }
    
    public int describeContents() {
        return 0;
    }
    
    @Override
    public byte[] encode() {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("message", getMessage());
            jsonObj.put("gift_id", getGift_id());
            jsonObj.put("gift_name", getGift_name());
            if (!TextUtils.isEmpty(getExtra()))
                jsonObj.put("extra", getExtra());
        } catch (JSONException e) {
            RLog.e("JSONException", e.getMessage());
        }
        try {
            return jsonObj.toString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelUtils.writeToParcel(dest, getMessage());
        ParcelUtils.writeToParcel(dest, getGift_id());
        ParcelUtils.writeToParcel(dest, getExtra());
        ParcelUtils.writeToParcel(dest, getGift_name());
    }
    
    public static final Creator<RGiftOpenTransientMessage> CREATOR = new Creator<RGiftOpenTransientMessage>() {
        @Override
        public RGiftOpenTransientMessage createFromParcel(Parcel source) {
            return new RGiftOpenTransientMessage(source);
        }
        @Override
        public RGiftOpenTransientMessage[] newArray(int size) {
            return new RGiftOpenTransientMessage[size];
        }
    };
    
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getGift_id() {
        return gift_id;
    }

    public void setGift_id(String gift_id) {
        this.gift_id = gift_id;
    }

    public String getGift_name() {
        return gift_name;
    }

    public void setGift_name(String gift_name) {
        this.gift_name = gift_name;
    }
}
