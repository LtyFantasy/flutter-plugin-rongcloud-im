package com.example.rongcloud_im_plugin.wp_message;

import android.os.Parcel;
import android.text.TextUtils;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;

import java.io.UnsupportedEncodingException;

import io.rong.common.ParcelUtils;
import io.rong.common.RLog;
import io.rong.imlib.MessageTag;
import io.rong.message.InformationNotificationMessage;

@MessageTag(value = "WP:UserKickout", flag = MessageTag.NONE)
public class RKickMessage extends InformationNotificationMessage {

    private String message;
    private long created_at;
    private String reason;

    public RKickMessage() {}

    public RKickMessage(Parcel in) {
        setMessage(ParcelUtils.readFromParcel(in));
        setCreated_at(ParcelUtils.readLongFromParcel(in));
        setReason(ParcelUtils.readFromParcel(in));
        setExtra(ParcelUtils.readFromParcel(in));
    }

    public RKickMessage(byte[] data) {
        String jsonStr = null;
        try {
            jsonStr = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            RLog.e("JSONException", e.getMessage());
        }
        RKickMessage rKickMessage = JSONObject.parseObject(jsonStr, RKickMessage.class);
        setMessage(rKickMessage.getMessage());
        setCreated_at(rKickMessage.getCreated_at());
        setReason(rKickMessage.getReason());
        setExtra(rKickMessage.getExtra());
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public byte[] encode() {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("message", getMessage());
            jsonObj.put("created_at", getCreated_at());
            jsonObj.put("reason", getReason());
            if (!TextUtils.isEmpty(getExtra()))
                jsonObj.put("extra", getExtra());
        } catch (JSONException e) {
            RLog.e("JSONException", e.getMessage());
        }
        try {
            return jsonObj.toString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelUtils.writeToParcel(dest, getMessage());
        ParcelUtils.writeToParcel(dest, getCreated_at());
        ParcelUtils.writeToParcel(dest, getReason());
        ParcelUtils.writeToParcel(dest, getExtra());
    }

    public static final Creator<RKickMessage> CREATOR = new Creator<RKickMessage>() {
        @Override
        public RKickMessage createFromParcel(Parcel source) {
            return new RKickMessage(source);
        }
        @Override
        public RKickMessage[] newArray(int size) {
            return new RKickMessage[size];
        }
    };

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(long created_at) {
        this.created_at = created_at;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

}
