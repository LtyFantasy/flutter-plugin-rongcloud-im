package com.example.rongcloud_im_plugin.wp_message;

import android.os.Parcel;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;

import java.io.UnsupportedEncodingException;

import io.rong.common.ParcelUtils;
import io.rong.common.RLog;
import io.rong.imlib.MessageTag;
import io.rong.message.InformationNotificationMessage;

@MessageTag(value = "WP:FlashChatCmd", flag = MessageTag.NONE)
public class RFlashChatCommandMessage extends InformationNotificationMessage {
    private static final String TAG = "RFlashChatCommandMessage";

    private int command;
    private String data;
    private RUserInfoBean user;

    public RFlashChatCommandMessage() {
    }

    public RFlashChatCommandMessage(Parcel in) {
        try {
            setCommand(ParcelUtils.readIntFromParcel(in));
            setData(ParcelUtils.readFromParcel(in));
            setUser(JSONObject.parseObject(ParcelUtils.readFromParcel(in), RUserInfoBean.class));
        } catch (Exception e) {
            RLog.e(TAG, e.getMessage());
        }
    }

    public RFlashChatCommandMessage(byte[] data) {
        String jsonStr = null;
        try {
            jsonStr = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            RLog.e("JSONException", e.getMessage());
        }
        RFlashChatCommandMessage rFlashChatCommandMessage = JSONObject.parseObject(jsonStr, RFlashChatCommandMessage.class);
        setCommand(rFlashChatCommandMessage.getCommand());
        setData(rFlashChatCommandMessage.getData());
        setUser(rFlashChatCommandMessage.getUser());
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public byte[] encode() {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("command", getCommand());
            jsonObj.put("data", getData());
            if (getUser() != null) jsonObj.put("user", getUser());
        } catch (JSONException e) {
            RLog.e("JSONException", e.getMessage());
        }
        try {
            return jsonObj.toString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelUtils.writeToParcel(dest, getCommand());
        ParcelUtils.writeToParcel(dest, getData());
        ParcelUtils.writeToParcel(dest, JSONObject.toJSONString(getUser()));
    }

    public static final Creator<RFlashChatCommandMessage> CREATOR = new Creator<RFlashChatCommandMessage>() {
        @Override
        public RFlashChatCommandMessage createFromParcel(Parcel source) {
            return new RFlashChatCommandMessage(source);
        }

        @Override
        public RFlashChatCommandMessage[] newArray(int size) {
            return new RFlashChatCommandMessage[size];
        }
    };

    public int getCommand() {
        return command;
    }

    public void setCommand(int command) {
        this.command = command;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public RUserInfoBean getUser() {
        return user;
    }

    public void setUser(RUserInfoBean user) {
        this.user = user;
    }
}
