package com.example.rongcloud_im_plugin.wp_message;

/**
 * Macx
 * 2019-11-27
 *
 * @Description:
 */
import android.os.Parcel;
import android.os.Parcelable;

import com.alibaba.fastjson.JSONObject;

import io.rong.common.ParcelUtils;
import io.rong.common.RLog;

public class RGiftMessageBean implements Parcelable{

    private String id;
    private String name;
    private String url;
    private int price;

    public RGiftMessageBean(){
    }

    public RGiftMessageBean(Parcel in){
        try {
            setId(ParcelUtils.readFromParcel(in));
            setName(ParcelUtils.readFromParcel(in));
            setUrl(ParcelUtils.readFromParcel(in));
            setPrice(ParcelUtils.readIntFromParcel(in));
        } catch (Exception e) {
            RLog.e("RGiftMessageBean", e.getMessage());
        }
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getUrl() {
        return url;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public int getPrice() {
        return price;
    }
    public void setPrice(int price) {
        this.price = price;
    }

    public static final Parcelable.Creator<RGiftMessageBean> CREATOR = new Creator<RGiftMessageBean>()
    {
        @Override
        public RGiftMessageBean[] newArray(int size)
        {
            return new RGiftMessageBean[size];
        }

        @Override
        public RGiftMessageBean createFromParcel(Parcel in)
        {
            return new RGiftMessageBean(in);
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel out, int paramInt) {
        ParcelUtils.writeToParcel(out, getId());
        ParcelUtils.writeToParcel(out, getName());
        ParcelUtils.writeToParcel(out, getUrl());
        ParcelUtils.writeToParcel(out, getPrice());
    }

}

