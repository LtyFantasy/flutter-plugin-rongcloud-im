package com.example.rongcloud_im_plugin.wp_message;

import android.os.Parcel;
import android.text.TextUtils;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;

import java.io.UnsupportedEncodingException;

import io.rong.common.ParcelUtils;
import io.rong.common.RLog;
import io.rong.imlib.MessageTag;
import io.rong.message.InformationNotificationMessage;

/**
 * Created by guotengqian on 2017/5/27 15:51.
 * def
 */
@MessageTag(value = "WP:SendGiftTip", flag = MessageTag.ISPERSISTED)
public class RSendGiftTipMessage extends InformationNotificationMessage {

    private String message;
    private int gender;
    public RSendGiftTipMessage() {}

    public RSendGiftTipMessage(Parcel in) {
        setMessage(ParcelUtils.readFromParcel(in));
        setExtra(ParcelUtils.readFromParcel(in));
        setGender(ParcelUtils.readIntFromParcel(in));
    }

    public RSendGiftTipMessage(byte[] data) {
        String jsonStr = null;
        try {
            jsonStr = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            RLog.e("JSONException", e.getMessage());
        }
        RSendGiftTipMessage rGiftOpenMessage = JSONObject.parseObject(jsonStr, RSendGiftTipMessage.class);
        setMessage(rGiftOpenMessage.getMessage());
        setExtra(rGiftOpenMessage.getExtra());
        setGender(rGiftOpenMessage.getGender());
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public byte[] encode() {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("message", getMessage());
            jsonObj.put("gender", getGender());
            if (!TextUtils.isEmpty(getExtra()))
                jsonObj.put("extra", getExtra());
        } catch (JSONException e) {
            RLog.e("JSONException", e.getMessage());
        }
        try {
            return jsonObj.toString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelUtils.writeToParcel(dest, getMessage());
        ParcelUtils.writeToParcel(dest, getExtra());
        ParcelUtils.writeToParcel(dest, getGender());
    }

    public static final Creator<RSendGiftTipMessage> CREATOR = new Creator<RSendGiftTipMessage>() {
        @Override
        public RSendGiftTipMessage createFromParcel(Parcel source) {
            return new RSendGiftTipMessage(source);
        }
        @Override
        public RSendGiftTipMessage[] newArray(int size) {
            return new RSendGiftTipMessage[size];
        }
    };

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static String getObjectName(){
        return "WP:SendGiftTip";
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }
}
