package com.example.rongcloud_im_plugin.wp_message;

import android.os.Parcel;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;

import java.io.UnsupportedEncodingException;

import io.rong.common.ParcelUtils;
import io.rong.common.RLog;
import io.rong.imlib.MessageTag;
import io.rong.imlib.model.MessageContent;

@MessageTag(value = "WP:GiftMsg", flag = MessageTag.ISCOUNTED | MessageTag.ISPERSISTED)
public class RGiftMessage extends MessageContent {

    private String giftId;
    private String content;
    private String extra;
    private int opened;  //1 opened, 0 unopen
    private RGiftMessageBean gift;
    private RUserInfoBean user;

    public RGiftMessage() {
    }

    /**
     * 构造函数。
     *
     * @param in 初始化传入的 Parcel。
     */
    public RGiftMessage(Parcel in) {
        try {
            setGiftId(ParcelUtils.readFromParcel(in));
            setContent(ParcelUtils.readFromParcel(in));
            setExtra(ParcelUtils.readFromParcel(in));
            setOpened(ParcelUtils.readIntFromParcel(in));
            setGift(JSONObject.parseObject(ParcelUtils.readFromParcel(in), RGiftMessageBean.class));
            setUser(JSONObject.parseObject(ParcelUtils.readFromParcel(in), RUserInfoBean.class));
        } catch (Exception e) {
            RLog.e("RGiftMessage", e.getMessage());
        }
    }

    /**
     * 构造函数。
     *
     * @param data 初始化传入的二进制数据。
     */
    public RGiftMessage(byte[] data) {
        String jsonStr = null;
        try {
            jsonStr = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            RLog.e("JSONException", e.getMessage());
        }

//        try {
//            org.json.JSONObject jsonObj = new org.json.JSONObject(jsonStr);
//
//            if (jsonObj.has("giftId"))
//                giftId = jsonObj.optString("giftId");
//
//        } catch (org.json.JSONException e) {
//
//        }
//        try {
//            new org.json.JSONObject(jsonStr);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        RGiftMessage rGiftMessage = JSONObject.parseObject(jsonStr, RGiftMessage.class);
        setContent(rGiftMessage.getContent());
        setExtra(rGiftMessage.getExtra());
        setOpened(rGiftMessage.getOpened());
        setGift(rGiftMessage.getGift());
        setGiftId(rGiftMessage.getGiftId());
        setUser(rGiftMessage.getUser());
    }

    /**
     * 描述了包含在 Parcelable 对象排列信息中的特殊对象的类型。
     *
     * @return 一个标志位，表明 Parcelable 对象特殊对象类型集合的排列。
     */
    public int describeContents() {
        return 0;
    }

    /**
     * 将本地消息对象序列化为消息数据。
     *
     * @return 消息数据。
     */
    @Override
    public byte[] encode() {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("giftId", getGiftId());
            jsonObj.put("content", getContent());

            jsonObj.put("extra", getExtra());
            jsonObj.put("opened", getOpened());
            jsonObj.put("gift", getGift());
            if (getUser() != null) jsonObj.put("user", getUser());
        } catch (JSONException e) {
            RLog.e("JSONException", e.getMessage());
        }
        try {
            return jsonObj.toString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 将类的数据写入外部提供的 Parcel 中。
     *
     * @param dest  对象被写入的 Parcel。
     * @param flags 对象如何被写入的附加标志，可能是 0 或 PARCELABLE_WRITE_RETURN_VALUE。
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelUtils.writeToParcel(dest, getGiftId());
        ParcelUtils.writeToParcel(dest, getContent());
        ParcelUtils.writeToParcel(dest, getExtra());
        ParcelUtils.writeToParcel(dest, getOpened());
        ParcelUtils.writeToParcel(dest, JSONObject.toJSONString(getGift()));
        ParcelUtils.writeToParcel(dest, JSONObject.toJSONString(getUser()));
    }

    /**
     * 读取接口，目的是要从Parcel中构造一个实现了Parcelable的类的实例处理。
     */
    public static final Creator<RGiftMessage> CREATOR = new Creator<RGiftMessage>() {
        @Override
        public RGiftMessage createFromParcel(Parcel source) {
            return new RGiftMessage(source);
        }

        @Override
        public RGiftMessage[] newArray(int size) {
            return new RGiftMessage[size];
        }
    };

    public String getGiftId() {
        return giftId;
    }

    public void setGiftId(String giftId) {
        this.giftId = giftId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public int getOpened() {
        return opened;
    }

    public void setOpened(int opened) {
        this.opened = opened;
    }

    public RGiftMessageBean getGift() {
        return gift;
    }

    public void setGift(RGiftMessageBean gift) {
        this.gift = gift;
    }

    public RUserInfoBean getUser() {
        return user;
    }

    public void setUser(RUserInfoBean user) {
        this.user = user;
    }

    public void setOpenedBoolean(boolean bool) {
        if (bool) {
            opened = 1;
        } else {
            opened = 0;
        }
    }

    public static String getObjectName() {
        return "WP:GiftMsg";
    }
}
