package com.example.rongcloud_im_plugin.wp_message;

import android.os.Parcel;
import android.text.TextUtils;


import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;

import java.io.UnsupportedEncodingException;

import io.rong.common.ParcelUtils;
import io.rong.common.RLog;
import io.rong.imlib.MessageTag;
import io.rong.message.InformationNotificationMessage;

/**
 * Created by guotengqian on 2017/1/6 21:10.
 * def
 */
@MessageTag(value = "WP:ConversationExtend", flag = MessageTag.NONE)
public class RExtendMessage extends InformationNotificationMessage {
    private String message;

    public RExtendMessage() {}

    public RExtendMessage(Parcel in) {
        setMessage(ParcelUtils.readFromParcel(in));
        setExtra(ParcelUtils.readFromParcel(in));
    }

    public RExtendMessage(byte[] data) {
        String jsonStr = null;
        try {
            jsonStr = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            RLog.e("JSONException", e.getMessage());
        }
        RExtendMessage rExtendMessage = JSONObject.parseObject(jsonStr, RExtendMessage.class);
        setMessage(rExtendMessage.getMessage());
        setExtra(rExtendMessage.getExtra());
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public byte[] encode() {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("message", getMessage());
            if (!TextUtils.isEmpty(getExtra()))
                jsonObj.put("extra", getExtra());
        } catch (JSONException e) {
            RLog.e("JSONException", e.getMessage());
        }
        try {
            return jsonObj.toString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelUtils.writeToParcel(dest, this.getMessage());
        ParcelUtils.writeToParcel(dest, this.getExtra());
    }

    public static final Creator<RExtendMessage> CREATOR = new Creator<RExtendMessage>() {
        @Override
        public RExtendMessage createFromParcel(Parcel source) {
            return new RExtendMessage(source);
        }
        @Override
        public RExtendMessage[] newArray(int size) {
            return new RExtendMessage[size];
        }
    };

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
