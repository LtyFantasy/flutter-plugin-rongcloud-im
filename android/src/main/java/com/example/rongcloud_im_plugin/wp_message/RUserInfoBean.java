package com.example.rongcloud_im_plugin.wp_message;

/**
 * Macx
 * 2019-11-27
 *
 * @Description:
 */
import android.os.Parcel;
import android.os.Parcelable;

import io.rong.common.ParcelUtils;
import io.rong.common.RLog;

public class RUserInfoBean implements Parcelable{
    private String userId;
    private String displayName;
    private int gender;
    private int age;
    private String address;

    public RUserInfoBean(){
    }

    public RUserInfoBean(Parcel in){
        try {
            setUserId(ParcelUtils.readFromParcel(in));
            setDisplayName(ParcelUtils.readFromParcel(in));
            setGender(ParcelUtils.readIntFromParcel(in));
            setAge(ParcelUtils.readIntFromParcel(in));
            setAddress(ParcelUtils.readFromParcel(in));
        } catch (Exception e) {
            RLog.e("RUserInfoBean", e.getMessage());
        }
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public static final Creator<RUserInfoBean> CREATOR = new Creator<RUserInfoBean>()
    {
        @Override
        public RUserInfoBean[] newArray(int size)
        {
            return new RUserInfoBean[size];
        }

        @Override
        public RUserInfoBean createFromParcel(Parcel in)
        {
            return new RUserInfoBean(in);
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }
    @Override
    public void writeToParcel(Parcel out, int paramInt) {
        ParcelUtils.writeToParcel(out, getUserId());
        ParcelUtils.writeToParcel(out, getDisplayName());
        ParcelUtils.writeToParcel(out, getGender());
        ParcelUtils.writeToParcel(out, getAge());
        ParcelUtils.writeToParcel(out, getAddress());
    }

}

