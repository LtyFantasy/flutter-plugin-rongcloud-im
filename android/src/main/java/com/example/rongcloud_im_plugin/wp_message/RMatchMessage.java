package com.example.rongcloud_im_plugin.wp_message;

import android.os.Parcel;
import android.text.TextUtils;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;

import java.io.UnsupportedEncodingException;

import io.rong.common.ParcelUtils;
import io.rong.common.RLog;
import io.rong.imlib.MessageTag;
import io.rong.message.InformationNotificationMessage;

@MessageTag(value = "WP:MatchNtf", flag = MessageTag.NONE)
public class RMatchMessage extends InformationNotificationMessage {

    private String message;
    private String targetId;
    private long created_at;
    private int[] interest;
    private RUserInfoBean user;

    public RMatchMessage() {
    }

    public RMatchMessage(Parcel in) {
        setMessage(ParcelUtils.readFromParcel(in));
        setTargetId(ParcelUtils.readFromParcel(in));
        setCreated_at(ParcelUtils.readLongFromParcel(in));
        setInterest(JSONObject.parseObject(ParcelUtils.readFromParcel(in), int[].class));
        setExtra(ParcelUtils.readFromParcel(in));
        setUser(JSONObject.parseObject(ParcelUtils.readFromParcel(in), RUserInfoBean.class));
    }

    public RMatchMessage(byte[] data) {
        String jsonStr = null;
        try {
            jsonStr = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            RLog.e("JSONException", e.getMessage());
        }
        RMatchMessage rMatchMessage = JSONObject.parseObject(jsonStr, RMatchMessage.class);
        setMessage(rMatchMessage.getMessage());
        setTargetId(rMatchMessage.getTargetId());
        setCreated_at(rMatchMessage.getCreated_at());
        setInterest(rMatchMessage.getInterest());
        setExtra(rMatchMessage.getExtra());
        setUser(rMatchMessage.getUser());
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public byte[] encode() {
        JSONObject jsonObj = new JSONObject();
        try {

            jsonObj.put("message", getMessage());
            if (!TextUtils.isEmpty(getTargetId())) jsonObj.put("targetId", getTargetId());
            jsonObj.put("created_at", getCreated_at());
            if (interest != null) jsonObj.put("interest", getInterest());
            if (!TextUtils.isEmpty(getExtra()))
                jsonObj.put("extra", getExtra());
            if (getUser() != null) jsonObj.put("user", getUser());
        } catch (JSONException e) {
            RLog.e("JSONException", e.getMessage());
        }
        try {
            return jsonObj.toString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelUtils.writeToParcel(dest, getMessage());
        ParcelUtils.writeToParcel(dest, getTargetId());
        ParcelUtils.writeToParcel(dest, getCreated_at());
        ParcelUtils.writeToParcel(dest, JSONObject.toJSONString(getInterest()));
        ParcelUtils.writeToParcel(dest, getExtra());
        ParcelUtils.writeToParcel(dest, JSONObject.toJSONString(getUser()));
    }

    public static final Creator<RMatchMessage> CREATOR = new Creator<RMatchMessage>() {
        @Override
        public RMatchMessage createFromParcel(Parcel source) {
            return new RMatchMessage(source);
        }

        @Override
        public RMatchMessage[] newArray(int size) {
            return new RMatchMessage[size];
        }
    };

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(long created_at) {
        this.created_at = created_at;
    }

    public int[] getInterest() {
        return interest;
    }

    public void setInterest(int[] interest) {
        this.interest = interest;
    }

    public RUserInfoBean getUser() {
        return user;
    }

    public void setUser(RUserInfoBean user) {
        this.user = user;
    }

    public static String getObjectName() {
        return "WP:MatchNtf";
    }
}
