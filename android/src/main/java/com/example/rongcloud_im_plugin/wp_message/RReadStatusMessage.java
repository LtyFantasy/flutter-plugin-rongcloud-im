package com.example.rongcloud_im_plugin.wp_message;

import android.os.Parcel;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;

import java.io.UnsupportedEncodingException;

import io.rong.common.ParcelUtils;
import io.rong.common.RLog;
import io.rong.imlib.MessageTag;
import io.rong.imlib.model.MessageContent;

/**
 * Created by guotengqian on 2017/4/27 10:40.
 * def
 */
@MessageTag(value = "WP:ReadStatus", flag = MessageTag.ISCOUNTED | MessageTag.ISPERSISTED)
public class RReadStatusMessage extends MessageContent {
    int status = 0; //0:已送达 1：已读

    public RReadStatusMessage(int status){
        this.status=status;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public RReadStatusMessage(byte[] data) {
        String jsonStr = null;

        try {
            jsonStr = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e1) {

        }
        JSONObject jsonObj = JSON.parseObject(jsonStr);
        status = jsonObj.getIntValue("status");
    }

    @Override
    public byte[] encode() {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("status", status);
        } catch (JSONException e) {
            RLog.e("JSONException", e.getMessage());
        }
        try {
            return jsonObj.toString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }


    public RReadStatusMessage(Parcel in) {
        status= ParcelUtils.readIntFromParcel(in);//该类为工具类，消息属性

        //这里可继续增加你消息的属性
    }

    /**
     * 读取接口，目的是要从Parcel中构造一个实现了Parcelable的类的实例处理。
     */
    public static final Creator<RReadStatusMessage> CREATOR = new Creator<RReadStatusMessage>() {

        @Override
        public RReadStatusMessage createFromParcel(Parcel source) {
            return new RReadStatusMessage(source);
        }

        @Override
        public RReadStatusMessage[] newArray(int size) {
            return new RReadStatusMessage[size];
        }
    };

    /**
     * 描述了包含在 Parcelable 对象排列信息中的特殊对象的类型。
     *
     * @return 一个标志位，表明Parcelable对象特殊对象类型集合的排列。
     */
    public int describeContents() {
        return 0;
    }

    /**
     * 将类的数据写入外部提供的 Parcel 中。
     *
     * @param dest  对象被写入的 Parcel。
     * @param flags 对象如何被写入的附加标志。
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        ParcelUtils.writeToParcel(dest, status);//该类为工具类，对消息中属性进行序列化
        //这里可继续增加你消息的属性
    }
}
