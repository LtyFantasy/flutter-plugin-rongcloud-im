package com.example.rongcloud_im_plugin;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import io.rong.push.PushType;
import io.rong.push.notification.PushMessageReceiver;
import io.rong.push.notification.PushNotificationMessage;

/**
 * Macx
 * 2019-12-18
 *
 * @Description:
 */
public class SealNotificationReceiver extends PushMessageReceiver {
    private static final String TAG = "SealNotification";
    private static final String RC_PUSH_TYPE_KEY = "push_type";
    private static final String RC_PUSH_CONTENT_KEY = "push_content";
    private static final String RC_PUSH_TYPE_VALUE = "RC_PUSH";

    @Override
    public boolean onNotificationMessageArrived(Context context, PushType pushType, PushNotificationMessage pushNotificationMessage) {
        return false;
    }

    @Override
    public boolean onNotificationMessageClicked(Context context, PushType pushType, PushNotificationMessage pushNotificationMessage) {
//        Intent intent = new Intent();
//        intent.setPackage(context.getPackageName());
//        context.startActivity(intent);

        String pushData = pushNotificationMessage.getPushData();
        Intent intent = new Intent(Intent.ACTION_MAIN);
        ComponentName componentName = new ComponentName("com.mason.wooplus", "com.mason.wooplus.MainActivity");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setComponent(componentName);
        intent.setAction("test");
        intent.putExtra(RC_PUSH_TYPE_KEY, RC_PUSH_TYPE_VALUE);
        intent.putExtra(RC_PUSH_CONTENT_KEY, pushData);
        context.startActivity(intent);
        return true;
    }
}
