package com.example.rongcloud_im_plugin.la_message;

import android.os.Parcel;
import android.text.TextUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.rong.common.ParcelUtils;
import io.rong.common.RLog;
import io.rong.imlib.MessageTag;
import io.rong.imlib.model.MentionedInfo;
import io.rong.imlib.model.MessageContent;
import io.rong.imlib.model.UserInfo;
import io.rong.message.TextMessage;

/**
 * Macx
 * 2020/10/15
 *
 * @Description:
 */
@MessageTag(value = "LA:CmdMsg", flag = MessageTag.ISCOUNTED | MessageTag.ISPERSISTED)
public class LaCmdMessage extends MessageContent {

    private static final String TAG = "LaCmdMessage";
    private String content;
    protected String extra;
    public static final Creator<LaCmdMessage> CREATOR = new Creator<LaCmdMessage>() {
        public LaCmdMessage createFromParcel(Parcel source) {
            return new LaCmdMessage(source);
        }

        public LaCmdMessage[] newArray(int size) {
            return new LaCmdMessage[size];
        }
    };

    public String getExtra() {
        return this.extra;
    }
    public JSONObject getExtraJsonObject() throws JSONException {
        try {
            return new JSONObject(this.extra);
        }catch (Exception e){
            return new JSONObject("{}");
        }

    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public byte[] encode() {
        JSONObject jsonObj = new JSONObject();

        try {
            jsonObj.put("content", this.getEmotion(this.getContent()));
            if (!TextUtils.isEmpty(this.getExtra())) {
                jsonObj.put("extra", this.getExtraJsonObject());
            }

            if (this.getJSONUserInfo() != null) {
                jsonObj.putOpt("user", this.getJSONUserInfo());
            }

            if (this.getJsonMentionInfo() != null) {
                jsonObj.putOpt("mentionedInfo", this.getJsonMentionInfo());
            }

            jsonObj.put("isBurnAfterRead", this.isDestruct());
            jsonObj.put("burnDuration", this.getDestructTime());
        } catch (JSONException var4) {
            RLog.e("LaCmdMessage", "JSONException " + var4.getMessage());
        }

        try {
            return jsonObj.toString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException var3) {
            RLog.e("LaCmdMessage", "UnsupportedEncodingException ", var3);
            return null;
        }
    }

    private String getEmotion(String content) {
        Pattern pattern = Pattern.compile("\\[/u([0-9A-Fa-f]+)\\]");
        Matcher matcher = pattern.matcher(content);

        StringBuffer sb;
        int inthex;
        for(sb = new StringBuffer(); matcher.find(); matcher.appendReplacement(sb, String.valueOf(Character.toChars(inthex)))) {
            String matchStr = matcher.group(1);
            inthex = 0;
            if (matchStr != null) {
                inthex = Integer.parseInt(matchStr, 16);
            }
        }

        matcher.appendTail(sb);
        return sb.toString();
    }

    protected LaCmdMessage() {
    }

    public static LaTextMessage obtain(String text) {
        LaTextMessage model = new LaTextMessage();
        model.setContent(text);
        return model;
    }

    public LaCmdMessage(byte[] data) {
        if (data == null) {
            RLog.e("LaCmdMessage", "data is null ");
        } else {
            String jsonStr = null;

            try {
                if (data.length >= 40960) {
                    RLog.e("LaCmdMessage", "TextMessage length is larger than 40KB, length :" + data.length);
                }

                jsonStr = new String(data, "UTF-8");
            } catch (UnsupportedEncodingException var5) {
                RLog.e("LaCmdMessage", "UnsupportedEncodingException ", var5);
            }

            if (jsonStr == null) {
                RLog.e("LaCmdMessage", "jsonStr is null ");
            } else {
                try {
                    JSONObject jsonObj = new JSONObject(jsonStr);
                    if (jsonObj.has("content")) {
                        this.setcontent(jsonObj.optString("content"));
                    }

                    if (jsonObj.has("extra")) {
                        this.setExtra(jsonObj.optString("extra"));
                    }

                    if (jsonObj.has("user")) {
                        this.setUserInfo(this.parseJsonToUserInfo(jsonObj.getJSONObject("user")));
                    }

                    if (jsonObj.has("mentionedInfo")) {
                        this.setMentionedInfo(this.parseJsonToMentionInfo(jsonObj.getJSONObject("mentionedInfo")));
                    }

                    if (jsonObj.has("isBurnAfterRead")) {
                        this.setDestruct(jsonObj.getBoolean("isBurnAfterRead"));
                    }

                    if (jsonObj.has("burnDuration")) {
                        this.setDestructTime(jsonObj.getLong("burnDuration"));
                    }
                } catch (JSONException var4) {
                    RLog.e("LaCmdMessage", "JSONException " + var4.getMessage());
                }

            }
        }
    }

    public void setcontent(String content) {
        this.content = content;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        ParcelUtils.writeToParcel(dest, this.getExtra());
        ParcelUtils.writeToParcel(dest, this.content);
        ParcelUtils.writeToParcel(dest, this.getUserInfo());
        ParcelUtils.writeToParcel(dest, this.getMentionedInfo());
        ParcelUtils.writeToParcel(dest, this.isDestruct() ? 1 : 0);
        ParcelUtils.writeToParcel(dest, this.getDestructTime());
    }

    public LaCmdMessage(Parcel in) {
        this.setExtra(ParcelUtils.readFromParcel(in));
        this.setcontent(ParcelUtils.readFromParcel(in));
        this.setUserInfo((UserInfo)ParcelUtils.readFromParcel(in, UserInfo.class));
        this.setMentionedInfo((MentionedInfo)ParcelUtils.readFromParcel(in, MentionedInfo.class));
        this.setDestruct(ParcelUtils.readIntFromParcel(in) == 1);
        this.setDestructTime(ParcelUtils.readLongFromParcel(in));
    }

    public String toString() {
        return "LaCmdMessage {content='" + this.content + '\'' + ", extra='" + this.extra + '\'' + '}';
    }

    public LaCmdMessage(String content) {
        this.setcontent(content);
    }

    public String getContent() {
        return this.content;
    }

    public List<String> getSearchableWord() {
        List<String> words = new ArrayList();
        words.add(this.content);
        return words;
    }
}
