package com.example.rongcloud_im_plugin;

import android.util.Log;

import androidx.annotation.NonNull;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.MethodChannel;

public class RongcloudImPlugin implements FlutterPlugin {
  private static MethodChannel channel;

  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "rongcloud_im_plugin");
    channel.setMethodCallHandler(RCIMFlutterWrapper.getInstance());
    RCIMFlutterWrapper.getInstance().saveContext(flutterPluginBinding.getApplicationContext());
    RCIMFlutterWrapper.getInstance().saveChannel(channel);
  }

  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
    Log.e("onDetachedFromEngine", "onDetachedFromEngine");
    channel = null;
  }
}
