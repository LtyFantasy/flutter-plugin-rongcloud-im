//
//  LABaseMessage.h
//  Pods-Runner
//
//  Created by wooplus on 2020/10/22.
//

#import <RongIMLib/RongIMLib.h>


NS_ASSUME_NONNULL_BEGIN

/**
    占卜师业务 基础消息类型
 */
@interface LABaseMessage : RCMessageContent

@property (nonatomic, copy) NSString *content;

@property (nonatomic, copy) NSString *extra;


@end

NS_ASSUME_NONNULL_END
