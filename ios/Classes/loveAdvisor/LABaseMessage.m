//
//  LABaseMessage.m
//  Pods-Runner
//
//  Created by wooplus on 2020/10/22.
//

#import "LABaseMessage.h"

@implementation LABaseMessage


+ (NSString *)getObjectName {
    return @"LA:BaseMsg";
}

- (NSData *)encode {
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    dict[@"content"] = _content ? _content : @"";
    dict[@"extra"] = _extra ? _extra : @"";
    NSData *data = [NSJSONSerialization dataWithJSONObject:dict
                                           options:kNilOptions
                                             error:nil];
    return data;
}

- (void)decodeWithData:(NSData *)data {
    
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    _content = dictionary[@"content"];
    _extra = dictionary[@"extra"];
}

+ (RCMessagePersistent)persistentFlag {
    return MessagePersistent_NONE;
}


@end
