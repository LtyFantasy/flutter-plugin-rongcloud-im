#import "RongcloudImPlugin.h"
#import "RCIMFlutterWrapper.h"
#import "RCFlutterConfig.h"
#import <RongIMLib/RongIMLib.h>

@implementation RongcloudImPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
    FlutterMethodChannel* channel = [FlutterMethodChannel
                                     methodChannelWithName:@"rongcloud_im_plugin"
                                     binaryMessenger:[registrar messenger]];
    RongcloudImPlugin* instance = [[RongcloudImPlugin alloc] init];
    [registrar addMethodCallDelegate:instance channel:channel];
    [registrar addApplicationDelegate:instance];
    [[RCIMFlutterWrapper sharedWrapper] addFlutterChannel:channel];
}



// app杀了后就移除回调， 为了处理杀死app情况下的崩溃 - (void)onConnectionStatusChanged:(RCConnectionStatus)status 方法在引擎销毁后还调用导致的崩溃，报：【Sending a message before the FlutterEngine has been run】 。  融云5.1.8插件解决了，但是因为我们使用的版本过老，只有仿着修改， 按照其git的提交记录，更改后，并没有生效。 所以这里直接通过问题核心本质来修改， 在app将要被杀时的回调里主动清理数据
- (void)applicationWillTerminate:(UIApplication *)application {
    [[RCIMFlutterWrapper sharedWrapper] addFlutterChannel:nil];
    [[RCIMClient sharedRCIMClient] setReceiveMessageDelegate:nil object:nil];
    [[RCIMClient sharedRCIMClient] setRCConnectionStatusChangeDelegate:nil];
}

- (void)handleMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {
    [[RCIMFlutterWrapper sharedWrapper] handleMethodCall:call result:result];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    NSString *token = [self getHexStringForData:deviceToken];
    NSLog(@"[RC:=========== device token: %@", token);
    [[RCIMFlutterWrapper sharedWrapper] setPushToken:token];
}

- (NSString *)getHexStringForData:(NSData *)data {
    NSUInteger len = [data length];
    char *chars = (char *)[data bytes];
    NSMutableString *hexString = [[NSMutableString alloc] init];
    for (NSUInteger i = 0; i < len; i ++) {
        [hexString appendString:[NSString stringWithFormat:@"%0.2hhx", chars[i]]];
    }
    return hexString;
}

@end
