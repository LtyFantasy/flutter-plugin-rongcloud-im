#import <Foundation/Foundation.h>

@interface NSDate (Format)
- (NSString *)wpf_getStringFromDateWithFormat:(NSString *)format;
@end
