#import "WPFRCMsgContent.h"
@class WPFUserInfo;
@class WPRCGiftCommodity;

@interface WPFRCGiftMsg : WPFRCMsgContent
@property (nonatomic, strong) NSString *giftId;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) WPRCGiftCommodity *commodity;
@property (nonatomic, assign) BOOL opened;

@property (nonatomic, strong) WPFUserInfo *user;
@end


@interface WPRCGiftCommodity : NSObject
@property (nonatomic, strong) NSString *commodityId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) CGFloat price;
@property (nonatomic, strong) NSURL *URL;
@end
