//
//  WPFRCFlashChatCMDMsg.h
//  rongcloud_im_plugin
//
//  Created by sol on 2022/3/29.
//

#import "WPFRCStatusMsg.h"

NS_ASSUME_NONNULL_BEGIN

@interface WPFRCFlashChatCMDMsg : WPFRCStatusMsg

@end

NS_ASSUME_NONNULL_END
