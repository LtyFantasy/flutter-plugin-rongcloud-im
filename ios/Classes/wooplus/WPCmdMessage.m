#import "WPCmdMessage.h"
#import "NSString+WPFlutter.h"

@implementation WPCmdMessage

+(instancetype)contentWithDataString:(NSString *)dataString
{
    if (![dataString isKindOfClass:[NSString class]]) {
        return nil;
    }
    if ([dataString wpf_isEmpty]) {
        return nil;
    }
    NSError *error = nil;
    NSData *data = [dataString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
    if (error) {
        return nil;
    }
    
    WPCmdMessage *msg = [[WPCmdMessage alloc] init];

    if (dic[@"name"]) {
        NSString *name = dic[@"name"];
        if ([name isEqualToString:@"WP:ConversationUnmatch"]) {
            msg.commandType = WPFCmdMessageTypeUnMatch;
        }
        if ([name isEqualToString:@"WP:MatchNtf"]) {
            msg.commandType = WPFCmdMessageTypeMatch;
        }
        if ([name isEqualToString:@"WP:ConversationRematch"]) {
            msg.commandType = WPFCmdMessageTypeRematch;
        }
        if ([name isEqualToString:@"WP:VIPConversationStartCommand"]) {
            msg.commandType = WPFCmdMessageTypeVIPStart;
        }
        if ([name isEqualToString:@"WP:UserKickout"]) {
            msg.commandType = WPFCmdMessageTypeUserKillOut;
        }
        if ([name isEqualToString:@"WP:ConversationExtend"]) {
            msg.commandType = WPFCmdMessageTypeExtend;
        }
    }
    
    if (dic[@"target_id"]) {
        msg.targetId = dic[@"target_id"];
    }
    if (dic[@"direction"]) {
        msg.direction = [dic[@"direction"] integerValue];
    }
    if (dic[@"send_at"]) {
        msg.sendAt = dic[@"send_at"];
    }
    if (dic[@"content"]) {
        msg.content = [WPCmdMsgContent contentWithDataString:dic[@"content"]];
    }
    return msg;
}


-(instancetype)init
{
    if (self = [super init]) {
        self.content = [[WPCmdMsgContent alloc] init];
    }
    return self;
}
@end
